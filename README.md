# FiniteStateMachine

<a href ="CHANGELOG.MD">Full changelog</a><br />

Version 0.3.0
- finally created package
- added StateBuilder
- added TransitionBuilder
- renamed ExitState to EndState to ensure that naming convention is identical everywhere
- added performing EnterState on StartState
- added fully preforming EndState

<i>Distributed under license <a href="LICENSE.MD">CreativeCommons 4.0 BY</a></i>