using System;
using FSM.States;

namespace FSM
{
    public class TransitionBuilder
    {
        private IState DestinationState;
        
        private IState SourceState;
        
        private Func<bool> Condition;
 
        private Func<float> DynamicDelay;

        private float Delay;
        
        public TransitionBuilder WithSource<TState>(TState sourceState)
            where TState : IState
        {
            SourceState = sourceState;
            return this;
        }
        
        public TransitionBuilder WithDestination<TState>(TState destinationState)
            where TState : IState
        {
            DestinationState = destinationState;
            return this;
        }

        public TransitionBuilder WithCondition(Func<bool> condition)
        {
            Condition = condition;
            return this;
        }

        public TransitionBuilder WithDynamicDelay(Func<float> dynamicDelayInMicroseconds)
        {
            DynamicDelay = dynamicDelayInMicroseconds;
            return this;
        }
        
        public TransitionBuilder WithStaticDelay(float delayInMicroseconds)
        {
            Delay = delayInMicroseconds;
            return this;
        }

        public Transition Build()
        {
            if (ReferenceEquals(DestinationState, null))
            {
                throw new NullReferenceException("Destination state is null! It is required for transition to work properly!");
            }

            if (ReferenceEquals(Condition, null))
            {
                throw new NullReferenceException("Condition state is null! It is required for transition to work properly!");
            }

            return Delay == 0
                ? new Transition(SourceState, DestinationState, DynamicDelay, Condition)
                : new Transition(SourceState, DestinationState, Delay, Condition);
        }
    }
}