using System;
using System.Collections.Generic;
using FSM.States;
using UnityEngine;

namespace FSM
{
    public class StateBuilder
    {
        public StateBuilder(FiniteStateMachine finiteStateMachine)
        {
            FiniteStateMachine = finiteStateMachine;
        }
        
        private readonly FiniteStateMachine FiniteStateMachine;

        private readonly List<ITransition> Transitions = new();

        private Action OnEnterStateEvent;
        private Action OnPerformStateEvent;
        private Action OnEndStateEvent;
        
        public StateBuilder WithTransition<TTransition>(TTransition transition)
            where TTransition : ITransition
        {
            if (!Transitions.Contains(transition))
            {
                Transitions.Add(transition);
            }

            return this;
        }

        public StateBuilder WithEnterStateEvent(Action enterStateEvent)
        {
            OnEnterStateEvent = enterStateEvent;

            return this;
        }

        public StateBuilder WithPerformStateEvent(Action performStateEvent)
        {
            OnPerformStateEvent = performStateEvent;

            return this;
        }

        public StateBuilder WithEndStateEvent(Action endStateEvent)
        {
            OnEndStateEvent = endStateEvent;

            return this;
        }

        public BaseState Build()
        {
            return new BaseState(FiniteStateMachine, OnEnterStateEvent, OnPerformStateEvent, OnEndStateEvent);
        }
    }
}