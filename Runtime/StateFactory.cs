﻿using System;
using FSM.States;

namespace FSM
{
    public static class StateFactory
    {
        public static IState GetState<T>(T type, params Transition[] transitions) where T : IState
        {
            switch (type)
            {
                //case MoveState state:
                //    AddTransitions(state, transitions);
                //    return state;
                //case AttackState state:
                //    AddTransitions(state, transitions);
                //    return state;
                //case DeathState state:
                //    AddTransitions(state, transitions);
                //    return state;
            }

            UnityEngine.Debug.Log("Couldn't recognize IState type, please add another case in switch above this line!");
            return null;
        }

        public static void AddTransitions<T>(T state, params Transition[] transitions) where T : IState
        {
            for (int i = 0; i < transitions.Length; i++)
            {
                state.AddTransition(transitions[i]);
            }
        }
    }
}
