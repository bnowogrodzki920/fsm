using System;
using FSM.States;

namespace FSM
{
    /// <summary>
    /// Base interface for all transitions
    /// </summary>
    public interface ITransition
    {
        /// <summary>
        /// State that FSM will be transitioned to
        /// </summary>
        public IState DestinationState { get; }
        /// <summary>
        /// State that FSM will be transitioned from
        /// </summary>
        public IState SourceState { get; }
        /// <summary>
        /// Transition condition
        /// </summary>
        /// <returns>
        /// True if condition is fulfilled
        /// </returns>
        public Func<bool> Condition { get; }

        /// <summary>
        /// Does this condition have a delay before executing transition?
        /// </summary>
        public bool HasDelay => Delay > 0;

        /// <summary>
        /// Delay that will be awaited <br />
        /// (works only if UseAsync in FSM is enabled!)
        /// </summary>
        public float Delay => 0;
    }
}