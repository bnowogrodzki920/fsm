﻿using System.Collections.Generic;

namespace FSM.States
{
    /// <summary>
    /// Base interface for all states
    /// </summary>
    public interface IState 
    {
        /// <summary>
        /// Called right after state was entered
        /// </summary>
        void EnterState();
        
        /// <summary>
        /// Called right before state is ended
        /// </summary>
        void EndState();
        
        /// <summary>
        /// Called on Tick - used for updating state logic and checking transitions
        /// </summary>
        void PerformState();
        
        /// <summary>
        /// Used for resetting state
        /// By default it is used right after EndState
        /// </summary>
        void ResetState();

        /// <summary>
        /// All transitions from this state
        /// </summary>
        IReadOnlyList<Transition> PossibleTransitions { get; }
        
        void AddTransition(Transition transition);
        void RemoveTransition(Transition transition);
    }
}
