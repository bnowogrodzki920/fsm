﻿using System;
using FSM.States;

namespace FSM
{
    /// <summary>
    /// Base class for all transitions
    /// </summary>
    public class Transition : ITransition
    {
        private IState destination;
        public IState DestinationState => destination;

        private IState source;
        public IState SourceState => source;

        private Func<bool> condition;
        public Func<bool> Condition => condition;
        
        private float delay;
        private Func<float> dynamicDelay;
        public float Delay 
        {
            get
            {
                if (delay == 0 && dynamicDelay != null)
                {
                    return dynamicDelay.Invoke();
                }
                
                return delay;
            }    
        }
        

        public Transition(IState source, IState destination, float delay, Func<bool> condition)
        {
            this.source = source;
            this.destination = destination;
            this.delay = delay;
            this.condition = condition;
        }
        
        public Transition(IState source, IState destination, Func<float> dynamicDelay, Func<bool> condition)
        {
            this.source = source;
            this.destination = destination;
            this.dynamicDelay = dynamicDelay;
            this.condition = condition;
        }
        
        public Transition(IState source, IState destination, Func<bool> condition)
        {
            this.source = source;
            this.destination = destination;
            this.delay = 0;
            this.condition = condition;
        }
        
        public Transition(IState destination, float delay, Func<bool> condition)
        {
            this.source = null;
            this.destination = destination;
            this.delay = delay;
            this.condition = condition;
        }
        
        public Transition(IState destination, Func<float> dynamicDelay, Func<bool> condition)
        {
            this.source = null;
            this.destination = destination;
            this.dynamicDelay = dynamicDelay;
            this.condition = condition;
        }
        
        public Transition(IState destination, Func<bool> condition)
        {
            this.source = null;
            this.destination = destination;
            this.delay = 0;
            this.condition = condition;
        }
    }
}
