﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace FSM.States
{
    /// <summary>
    /// Base class for all states
    /// </summary>
    public class BaseState : IState
    {
        public BaseState(FiniteStateMachine stateMachine)
        {
            this.stateMachine = stateMachine;
        }
        
        public BaseState(FiniteStateMachine stateMachine,
            Action onEnterStateEvent,
            Action onPerformStateEvent,
            Action onEndStateEvent)
        {
            this.stateMachine = stateMachine;
            OnEnterStateEvent = onEnterStateEvent;
            OnPerformStateEvent = onPerformStateEvent;
            OnEndStateEvent = onEndStateEvent;
        }
        
        protected FiniteStateMachine stateMachine = null;

        protected List<Transition> possibleTransitions = new List<Transition>();
        public IReadOnlyList<Transition> PossibleTransitions => possibleTransitions;

        public event Action OnEnterStateEvent;
        public event Action OnPerformStateEvent;
        public event Action OnEndStateEvent;

        public virtual void EnterState() 
        { 
            OnEnterStateEvent?.Invoke();
        }
        
        public virtual void EndState() 
        {
            OnEndStateEvent?.Invoke();
            ResetState();
        }

        public virtual void PerformState()
        {
            OnPerformStateEvent?.Invoke();
        }

        public virtual void ResetState()
        {
        }

        public void AddTransition(Transition transition)
        {
            if (!possibleTransitions.Contains(transition))
            {
                possibleTransitions.Add(transition);
            }
        }

        public void RemoveTransition(Transition transition)
        {
            if (possibleTransitions.Contains(transition))
            {
                possibleTransitions.Remove(transition);
            }
        }
    }
}
