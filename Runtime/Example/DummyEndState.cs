using FSM.States;
using UnityEngine;

namespace FSM.Example
{
    public class DummyEndState : BaseState
    {
        public DummyEndState(FiniteStateMachine stateMachine) : base(stateMachine)
        {
        }

        public override void EnterState()
        {
            Debug.Log($"[{nameof(DummyEndState)}.{nameof(EnterState)}] Entered ExitState");
        }
        
        public override void PerformState()
        {
            Debug.Log($"[{nameof(DummyEndState)}.{nameof(EnterState)}] Performing ExitState");
        }

        public override void EndState()
        {
            Debug.Log($"[{nameof(DummyEndState)}.{nameof(EnterState)}] Ended ExitState");
        }
    }
}