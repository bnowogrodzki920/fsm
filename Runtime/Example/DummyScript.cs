using System;
using FSM.States;
using UnityEngine;

namespace FSM.Example
{
    public class DummyScript : MonoBehaviour
    {
        private FiniteStateMachine fsm;

        [ContextMenu("FSM With Transition Builder")]
        private void ClassicStart()
        {
            fsm = new FiniteStateMachine();
            var startState = new DummyStartState(fsm);
            var endState = new DummyEndState(fsm);
            var midState = new DummyMidState(fsm);

            var currentTime = Time.time;
            var timeOffset = 3;

            var startToMidTransition = new TransitionBuilder()
                .WithSource(startState)
                .WithDestination(midState)
                .WithCondition(() => currentTime + timeOffset < Time.time)
                .Build();

            var midToEndTransition = new TransitionBuilder()
                .WithSource(midState)
                .WithDestination(endState)
                .WithCondition(() => currentTime + timeOffset + timeOffset < Time.time)
                .Build();

            startState.AddTransition(startToMidTransition);
            midState.AddTransition(midToEndTransition);
            
            fsm.TryAddStartState(startState);
            fsm.TryAddEndState(endState);
            fsm.TryAddState(midState);
        }

        private void Update()
        {
            fsm?.Tick();
        }
    }
}
