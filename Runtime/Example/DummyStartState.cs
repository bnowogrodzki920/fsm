using FSM.States;
using UnityEngine;

namespace FSM.Example
{
    public class DummyStartState : BaseState
    {
        public DummyStartState(FiniteStateMachine stateMachine) : base(stateMachine)
        {
        }
        
        public override void EnterState()
        {
            Debug.Log($"[{nameof(DummyStartState)}.{nameof(EnterState)}] Entered StartState");
        }
        
        public override void PerformState()
        {
            Debug.Log($"[{nameof(DummyStartState)}.{nameof(EnterState)}] Performing StartState");
        }

        public override void EndState()
        {
            Debug.Log($"[{nameof(DummyStartState)}.{nameof(EnterState)}] Ended StartState");
        }
    }
}