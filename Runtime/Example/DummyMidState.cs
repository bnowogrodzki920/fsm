using FSM.States;
using UnityEngine;

namespace FSM.Example
{
    public class DummyMidState : BaseState
    {
        public DummyMidState(FiniteStateMachine stateMachine) : base(stateMachine)
        {
        }        
        
        public override void EnterState()
        {
            Debug.Log($"[{nameof(DummyMidState)}.{nameof(EnterState)}] Entered MidState");
        }
        
        public override void PerformState()
        {
            Debug.Log($"[{nameof(DummyMidState)}.{nameof(EnterState)}] Performing MidState");
        }

        public override void EndState()
        {
            Debug.Log($"[{nameof(DummyMidState)}.{nameof(EnterState)}] Ended MidState");
        }
    }
}