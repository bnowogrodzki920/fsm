﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FSM.States;
using UnityEngine;

namespace FSM
{
    /// <summary>
    /// Main class for FiniteStateMachine
    /// </summary>
    public class FiniteStateMachine
    {
        private List<IState> states = new List<IState>();
        private List<ITransition> anyStateTransitions = new List<ITransition>();

        private IState startState;
        private IState currentState;
        private IState endState;

        #region Getters
        /// <summary>
        /// State that FSM will start in
        /// </summary>
        public IState StartState => startState;
        
        /// <summary>
        /// Currently running state
        /// </summary>
        public IState CurrentState => currentState;
        
        /// <summary>
        /// State that FSM will end in
        /// </summary>
        public IState EndState => endState;
        
        /// <summary>
        /// All states count
        /// </summary>
        public int StateCount => states.Count;

        /// <summary>
        /// Should send Logs into UnityConsole?
        /// </summary>
        public bool IgnoreLogs { get; set; } = true;
        
        /// <summary>
        /// Should use asynchronous tick?
        /// </summary>
        public bool UseAsync { get; set; } = true;
        #endregion

        private bool duringUpdate;
        private bool inited;
        private bool finished;
        
        /// <summary>
        /// Tries to add StartState
        /// </summary>
        /// <param name="state">State to add as StartState</param>
        /// <param name="forceSetStartState">Should override StartState?</param>
        /// <returns>True if state was added properly</returns>
        public bool TryAddStartState<TState>(TState state, bool forceSetStartState = false)
            where TState : IState
        {
            if(forceSetStartState || (ReferenceEquals(startState, null) && !states.Contains(state)))
            {
                states.Add(state);
                startState = state;
                currentState = state;
                return true;
            }

            if (!IgnoreLogs)
            {
                Debug.LogError($"[{nameof(FiniteStateMachine)}.{nameof(TryAddStartState)}] StartState has already been added.");
            }
                
            return false;
        }
        
        /// <summary>
        /// Tries to add EndState
        /// </summary>
        /// <param name="state">State to add as EndState</param>
        /// <param name="forceSetEndState">Shou override EndState?</param>
        /// <returns>True if state was added properly</returns>
        public bool TryAddEndState<TState>(TState state, bool forceSetEndState = false)
            where TState : IState
        {
            if(forceSetEndState || (ReferenceEquals(endState, null) && !states.Contains(state)))
            {
                states.Add(state);
                endState = state;
                return true;
            }

            if (!IgnoreLogs)
            {
                Debug.LogError($"[{nameof(FiniteStateMachine)}.{nameof(TryAddEndState)}] EndState has already been added.");
            }

            return false;
        }  
        
        /// <summary>
        /// Tries to add new State
        /// </summary>
        /// <param name="state">State to add</param>
        /// <returns>True if state was added properly</returns>
        public bool TryAddState<TState>(TState state)
            where TState : IState
        {
            if(!states.Contains(state))
            {
                states.Add(state);
                return true;
            }
            
            if (!IgnoreLogs)
            {
                Debug.LogError($"[{nameof(FiniteStateMachine)}.{nameof(TryAddState)}] State has already been added.");
            }
            
            return false;
        }
        
        /// <summary>
        /// Tries to remove new State
        /// </summary>
        /// <param name="state">State to remove</param>
        /// <returns>True if state was removed properly</returns>
        public bool TryRemoveState<TState>(TState state)
            where TState : IState
        {
            if (states.Remove(state))
            {
                return true;
            }
            
            if (!IgnoreLogs)
            {
                Debug.LogError($"[{nameof(FiniteStateMachine)}.{nameof(TryAddEndState)}] EndState has already been removed or it never was added.");
            }
            
            return false;
        }

        /// <summary>
        /// Tries to add transition to <b>ANY</b> state
        /// </summary>
        /// <param name="transition">Transition to add</param>
        /// <returns>True if transition was properly added</returns>
        public bool TryAddTransitionToAnyState<TTransition>(TTransition transition)
            where TTransition : ITransition
        {
            if(!anyStateTransitions.Contains(transition))
            {
                anyStateTransitions.Add(transition);
                return true;
            }

            if (!IgnoreLogs)
            {
                Debug.LogError($"[{nameof(FiniteStateMachine)}.{nameof(TryAddTransitionToAnyState)}] Transition has already been added.");
            }
            
            return false;
        }

        /// <summary>
        /// Try set state manually (ignore entering condition)
        /// If state is not added - adds it to the states
        /// </summary>
        /// <param name="state">State to transit manually.</param>
        /// <returns>True if state is properly set.</returns>
        public bool TrySetStateManually<TState>(TState state)
            where TState : IState
        {
            if (ReferenceEquals(state, null))
            {
                return false;
            }
            
            var cachedIgnoreLogs = IgnoreLogs;
            IgnoreLogs = false;
            
            if (TryAddState(state))
            {
                states.Add(state);
            }
            
            IgnoreLogs = cachedIgnoreLogs;
            
            if (state.Equals(currentState))
            {
                if (!IgnoreLogs)
                {
                    Debug.LogError($"[{nameof(FiniteStateMachine)}.{nameof(TrySetStateManually)}] State machine is already in this state.");
                }
                
                return false;
            }
            
            MoveToNextState(state);
            return true;
        }

        /// <summary>
        /// Updates FiniteStateMachine
        /// </summary>
        public async void Tick()
        {
            if (finished)
            {
                return;
            }

            if (duringUpdate)
            {
                return;
            }
            
            if (CheckIfEndState())
            {
                return;
            }

            if (currentState == startState && !inited)
            {
                currentState.EnterState();
                inited = true;
            }
            
            duringUpdate = true;

            if (UseAsync)
            {
                await TryToMoveToNextState();
            }
            else
            {
                #pragma warning disable CS4014
                TryToMoveToNextState();
                #pragma warning restore CS4014
            }
            
            currentState.PerformState();
            duringUpdate = false;
        }

        private bool CheckIfEndState()
        {
            if (currentState != endState)
            {
                return false;
            }
            
            if (currentState.PossibleTransitions.Count > 0 && currentState.PossibleTransitions[0].Condition.Invoke())
            {
                currentState.PerformState();
            }
            else
            {
                currentState.EndState();
                finished = true;
            }

            return true;

        }

        private async Task<bool> TryToMoveToNextState()
        {
            foreach(ITransition transition in anyStateTransitions)
            {
                if (transition.Condition.Invoke())
                {
                    if (transition.HasDelay)
                    {
                        if (UseAsync)
                        {
                            await Task.Delay(Mathf.FloorToInt(transition.Delay * 1000));
                        }

                        if (!IgnoreLogs)
                        {
                            Debug.Log($"[{nameof(FiniteStateMachine)}.{nameof(TryToMoveToNextState)}] Waited for {transition.Delay * 1000} before entering {transition.DestinationState} State");
                        }
                    }
                    
                    MoveToNextState(transition.DestinationState);
                    return true;
                }
            }

            foreach (ITransition transition in currentState.PossibleTransitions)
            {
                if (transition.Condition.Invoke())
                {
                    if (transition.HasDelay)
                    {
                        if (UseAsync)
                        {
                            await Task.Delay(Mathf.FloorToInt(transition.Delay * 1000));
                        }

                        if (!IgnoreLogs)
                        {
                            Debug.Log($"[{nameof(FiniteStateMachine)}.{nameof(TryToMoveToNextState)}] Waited for {transition.Delay * 1000} before entering {transition.DestinationState} State");
                        }
                    }
                        
                    MoveToNextState(transition.DestinationState);
                    return true;
                }
            }
            
            return false;
        }

        private void MoveToNextState(IState state)
        {
            currentState?.EndState();
            currentState = state;
            currentState.EnterState();
        }
    }
}
